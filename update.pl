#!/usr/bin/perl

mkdir '/etc/bind/slave/' unless -d '/etc/bind/slave/';

my $def = '123.45.67.89'; # your primary dns by default

open F, '>/etc/bind/named.conf.slaves';
open D, '/etc/bind/list';
while (<D>) {
	s/[\r\n]//g;
	s/^\s//g;
	s/\s$//g;
	my ($d, $ip) = split /\s+/, $_, 2;
	next unless $d =~ m/^[-a-z0-9\.]+$/;
	$ip = $def unless $ip =~ m/^\d+\.\d+\.\d+\.\d+$/;

	printf F 'zone "%s" {
	type slave;
	file "/etc/bind/slave/%s";
	masters { %s; };
};

', $d, $d, $ip;

}
close D;
close F;
`/etc/init.d/bind9 reload`;
